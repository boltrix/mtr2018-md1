package com.example.boltrix.mtr2018_md1;

import android.Manifest;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.content.pm.PackageManager;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AUDIOFragment extends Fragment {


    private static final String TAG = "AUDIOFragment";
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private MediaRecorder mRecorder = null;
    private static String mFileName = null;
    private static String FileName = null;
    private Button btnTAB;
    private Boolean ispressed = false;
    private LayoutInflater inflater;
    private boolean permissionToRecordAccepted = false;
    private String [] permissions = {Manifest.permission.RECORD_AUDIO,Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE};





    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted ) getActivity().finish();;

    }


    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e(TAG, "Kļūda 1");
        }

        mRecorder.start();
    }

    private void stopRecording() {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestPermissions(permissions, REQUEST_RECORD_AUDIO_PERMISSION);
    }


     public void uniqueFilename(){
        //Get unique filename
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.US);
        Date now = new Date();
        FileName = formatter.format(now) + ".3gp";
        // Record to the external cache directory for visibility
        mFileName = getActivity ().getExternalCacheDir ().getAbsolutePath();
        mFileName += "/"  + FileName;
     }

    @Override
    public void onStop() {
        super.onStop();
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.audio_fragment,container,false);
       btnTAB = (Button) view.findViewById(R.id.btnAUDIO);
       btnTAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (!ispressed) {
                   uniqueFilename();
                startRecording();
                Toast.makeText(getActivity(), "Ieraksts sācies" ,Toast.LENGTH_SHORT).show();
                TextView textView = (TextView) getView().findViewById (R.id.file_name);
                textView.setText(FileName);
                ispressed = true;}
                else {
                   stopRecording();
                   Toast.makeText(getActivity(), "Ieraksts beidzies" ,Toast.LENGTH_SHORT).show();
                   ispressed = false;
               }
            }
        });

        return view;
    }



}
